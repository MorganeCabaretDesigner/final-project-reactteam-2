import React from "react";
import "../../Assets/Styles/styles.css";
import "./lesson-creation.css";
import AddContent from "./Add-content-component/add-content-component";
import LessonTitle from "./lesson-title-component";


const LessonContent = () => {
    return (
        <div className="container container-lesson background-white py-4 my-4 px-0">
            <LessonTitle />
            <AddContent />
        </div>
    )
}

export default LessonContent;
