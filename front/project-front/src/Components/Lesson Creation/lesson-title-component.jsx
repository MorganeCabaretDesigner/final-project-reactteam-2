import React, {useState} from "react";
import "../../Assets/Styles/styles.css";
import "./lesson-creation.css";
import ButtonSave from "../Buttons/button-save-component";


const LessonTitle = () => {

    const [title, setTitle] = useState("");
    const [currentTitle, setCurrentTitle] = useState("");

    const onChangeTitle = (e) => {
        const title = e.target.value;
        setTitle(title);
    };

    const handleSave = () => {

    }

    const updateTitle = () => {

    }

    return (
        <div className="line-bottom">
            <div className="container-fluid background-white">
                <div className="row py-4 mx-3">
                    {currentTitle ? (
                        <>
                            {title}
                        </>
                    ) : (
                        <>
                            <input className="form-control inputs-zones title-lesson-input mx-2"
                                   name="title"
                                   type="text"
                                   placeholder="Nom de la lesson ..."
                                   value={title}
                                   onChange={onChangeTitle}
                            />
                            <ButtonSave onClick={handleSave}/>
                        </>
                    )}
                </div>
            </div>
        </div>
    )
}

export default LessonTitle;
