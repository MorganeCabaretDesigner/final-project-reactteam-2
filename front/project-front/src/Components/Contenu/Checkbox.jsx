import React from 'react';


export default class Checkbox extends React.Component {
    constructor(params) {
        super(params)
        // initial gender state set from props
        this.state = {
            position: this.props.position
        }
        this.setPosition = this.setPosition.bind(this)
    }

    setPosition(event) {
        this.setState({
            position: event.target.value
        })
    }

    render() {
        const {position} = this.state
        return <div>
            <div>
                <input type="radio" checked={position === "Droite"}
                       onClick={this.setPosition} value="Droite"/>
                <label htmlFor="box-right" className="custom-control-label"> Droite </label>

                <input type="radio" checked={position === "Gauche"}
                       onClick={this.setPosition} value="Gauche"/>
                <label htmlFor="box-left" className="custom-control-label"> Gauche </label>
            </div>
        </div>;
    }
}

/*
 * Render the above component into the div#app
 */
