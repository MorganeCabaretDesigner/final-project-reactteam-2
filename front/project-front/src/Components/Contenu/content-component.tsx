import React, {Component, useState} from 'react';

import "../../Assets/Styles/styles.css";
import "./Stylescontenu.css";
import ButtonSave from "../Buttons/button-save-component";

import './Dropzone-component/dropzone-content-img.css';

import DragAndDrop from "./Dropzone-component/DragAndDrop";

const Checkbox = () => {
    const [state,setState]=useState('Droite');
    const handleChange=(event:any)=>{
        setState(event.target.value)
        console.log(event.target.value)
    }
    return (
        <div className="row">
<div  className=" col 1 offset-4">

            <input className="form-check-input " type="radio" name="position-image"  value="Droite"  checked={state==="Droite"} onChange={handleChange}/>
            <label htmlFor="box-left " className=" form-check-label text-checkbox-black  "> Droite </label>
</div>
<div className=" col-1 px-1">
            <input  className="form-check-input me-4" type="radio"name="position-image"   value="Gauche" checked={state==="Gauche"}  onChange={handleChange} />
            <label htmlFor="box-left "  className=" form-check-label  text-checkbox-grey  "> Gauche </label>
        </div>
        </div>


    );
};
export class Content extends Component<any, any> {


   state = {
        Contents: [
            {title: 'string', contenu: 'string'}
        ],

        radio: 'boolean',
        nouveauTitle: '',
        nouveauContenu: '',
        color: '#FFFFF',
        nouveauColor: '',

    };


handleSubmit = (event: any) => {
        event.preventDefault();
        const title = this.state.nouveauTitle;
        const contenu = this.state.nouveauContenu;
        const contents1 = {title: title, contenu: contenu};

        const Contents = this.state.Contents.slice();
        Contents.push(contents1);
        this.setState({Contents: Contents, nouveauContenu: '', nouveauTitle: ''});


        const data=JSON.stringify(this.state)
        this.setState({
            title:'',
            contenu:'',
            color:'',

        })
        console.log (data)
        console.log(title,contenu);

    }


    handleChange = (event: any) => {
        const value1 = event.currentTarget.value1;
        this.setState({nouveauTitle:value1});
        const value2= event.currentTarget.value2;
        this.setState({nouveauContenu: value2})
        const color = event.currentTarget.color;
        this.setState({nouveauColor: color});

       console.log(event.currentTarget.value);

    }


    render() {

        return (
            <div className="line-bottom">
                <div className="container">
                    <div className="row justify-content-center my-3">
                        <h1 className="title-blue">Contenu</h1>
                    </div>

                    <div className='row  mb-2'>
                        <div className='col-5 offset-1'>
                            <div className="row row-content-title">
                                <input value={this.state.nouveauTitle}
                                       onChange={this.handleChange} className="inputs-zones input-title " type="text " placeholder="Titre..."/>
                            </div>
                            <div className="row row-content-big">
                                <textarea value={this.state.nouveauContenu} onChange={this.handleChange}
                                          className="inputs-zones input-content-big"
                                          placeholder="Contenu..."/>
                            </div>
                        </div>
                        <div className="col-5 big-row" >

                            <DragAndDrop/>



                        </div>
                    </div>

                    <div className="row">
                        <div className="col-4 offset-1">
                            <p className="text-couleur d-inline"> Couleur de fond
                                <input  value={this.state.nouveauColor} onChange={this.handleChange} className="text-placeholder  inputs-zones mx-2 " placeholder="#KM36PI..."/>
                            </p>

                        </div>

                            <div className="row col-5 offset-2 ">

                                <p className="text-position mt-1"> Positionnement image</p>

                             <Checkbox />

                            </div>

                    </div>

                    <div className=" row justify-content-center mb-2 " onClick={this.handleSubmit}>
                        <ButtonSave/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Content;
