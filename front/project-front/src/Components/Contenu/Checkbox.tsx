import React, {useState} from 'react';

import '../../Components/Contenu/Stylescontenu.css';

export const Checkbox = () => {
    const [state,setState]=useState('Droite');
    const handleChange=(event:any)=>{
        setState(event.target.value)
        console.log(event.target.value)
    }
    return (
            <div >
            <div className=" row col-1 form-check form-check-inline right ">
            <input className="form-check-input" type="radio" name="position-image" value="Droite" checked={state==="Droite"} onChange={handleChange}/>
            <label htmlFor="box-right " className="text-checkbox-black"> Droite </label>
            </div>
                <div className="col-1 form-check form-check-inline ">
            <input className="form-check-input" type="radio" name="position-image"  value="Gauche" checked={state==="Gauche"} onChange={handleChange} />
            <label htmlFor="box-left" className="text-checkbox-grey"> Gauche </label>
            </div>
        </div>
    );
};

export default Checkbox ;
