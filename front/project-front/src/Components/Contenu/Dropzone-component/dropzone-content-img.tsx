import React, {useMemo} from 'react';
import {useDropzone} from 'react-dropzone';
import './dropzone-content-img.css'

function DropzoneContentImg() {

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({accept: 'image/*'});

    const style: any = useMemo(() => ({

    }), [
        isDragActive,
        isDragReject,
        isDragAccept
    ]);

    return (
        <div className="dropbox-img" {...getRootProps({style})}>
            <input {...getInputProps()} />
            <p className="Box"> + Ajout Image </p>
        </div>
    );
}

export default DropzoneContentImg;
