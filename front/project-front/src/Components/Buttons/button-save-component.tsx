import React from "react";
import "../../Assets/Styles/styles.css";
import axios from "axios";
const api = 'http://localhost:8080/lesson/';

const ButtonSave = () => {


    const [send, setSend] = React.useState({
        content: {
            title: "content",
            color: "blue",
            img : "aa"
        },
        video: {
            title: "video",
            youtube : "aa",
            mp4: "aa"
        },
        quiz : {
            title: "quiz",
            question: "oo",
            answers: "integer"

        }
    })
    React.useEffect(() => {
        async function AddSave () {
            console.log('function')


             try  {
                 console.log('enregistre')

                await axios.post(api, send)

            }
            catch(e){
                setSend(e.message)
            }
        }
        AddSave();
    }, [])


    return (

        <button type="submit" className="button-black mx-2">
            <div className="text-button">Enregistrer</div>
        </button>
    )
}

export default ButtonSave;
