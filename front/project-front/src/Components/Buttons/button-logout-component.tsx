import AuthService from "../../Services/auth.service";
import React from "react";

const ButtonLogOut = () => {
    return (
        <>
            <button className="button-white mx-2 col-1">
                <div onClick={AuthService.logout}>Déconnexion</div>
            </button>
        </>
    )
};

export default ButtonLogOut;


