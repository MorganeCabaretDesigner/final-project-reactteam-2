import React, {useState} from "react";
import ButtonSave from "../Buttons/button-save-component";
import DragAndDrop from "./DragAndDrop";
import "./video-style.css";
import "../../Assets/Styles/styles.css";
import "./dropzone-video.css";


export const Video = () => {

  const [title, setTitle] = useState('');
  const [video, setVideo] = useState('');

  const handleTitle = (e: any) => {
    setTitle(e.target.value)
    console.log(title);
  };

  const handleVideo = (e: any) => {
    setVideo(e.target.value)
    console.log(video);
  };
  const handleSubmit = (e: any) =>{
    e.preventDefault();
    console.log(title, video, DragAndDrop);
  }

    return <div className="container" >


      <h1 className="title-blue d-flex justify-content-center"> Vidéo  </h1>

      <div className='row'>

        <form className='col-10 offset-1  p-0'  >
          <input className="form-control inputs-zones my-3 onSubmit={handleSubmit}"
                 type="text"
                 placeholder="Titre Vidéo.."
                 onChange={handleTitle}
                 value={title}


          />
          <input  className="form-control inputs-zones"
                  type="text"
                  placeholder="Ajout vidéo Youtube"
                  onChange={handleVideo}
                  value={video}

          />
        </form>

      </div>

      <div className='row d-flex justify-content-center' >
        <div className='col-10 my-3 p-0 '>
          <DragAndDrop/>



        </div>
      </div>

            <div className="row justify-content-center my-5" onClick={handleSubmit}  >
                <ButtonSave/>
            </div>

      <div className="line-bottom"> </div>
      </div>



}
