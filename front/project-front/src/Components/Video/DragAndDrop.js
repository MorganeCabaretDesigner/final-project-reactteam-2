import React, {useMemo, useEffect, useState} from 'react';
import {useDropzone} from 'react-dropzone';
import './dropzone-video.css';


function DragAndDrop() {

    const [files, setFiles] = useState([]);

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject,

    } = useDropzone({accept : 'video/*', onDrop: acceptedFiles => {
            setFiles(acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file),
            })));
        }});

    const style = useMemo(() => ({

    }), [
        isDragActive,
        isDragReject,
        isDragAccept
    ]);


    const thumb = {

        width: "100%",
        height: "100%",
    };


    const preview = {
        width: '100%',
        height: '100%',
    };

    const thumbs = files.map(file => (
        <div style={thumb} key={file.name} >
            <div className="col-12 d-flex justify-content-center">
                <video
                    src={file.preview}
                    style={preview}
                />
            </div>
        </div>
    ));

    useEffect(() => () => {
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);


    React.useEffect(() => {
        const textInput = document.getElementById('text');
        const fileInput = document.getElementById('inputFile');
        const eventEmitter = (event) => {
            if (event != null) {
                textInput.style.display = 'none';
            }
        }
        fileInput.addEventListener('change', eventEmitter);
        return () => {
            fileInput.removeEventListener('change', eventEmitter);
        }
    }, []);


    return (
        <div>
            <div className="dropbox-video" {...getRootProps({style})}>
                <input {...getInputProps()} type='file' id="inputFile"   />

                <p className="Box-video" id="text"> + Ajouter Video MP4</p>

                <aside style={thumb}>
                    {thumbs}
                </aside>

            </div>
        </div>
    );
}


export default DragAndDrop;


