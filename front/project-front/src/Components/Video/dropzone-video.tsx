import React, {useMemo} from 'react';
import {useDropzone} from 'react-dropzone';
import './dropzone-video.css';

function Dropzone(props: any) {

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({accept: 'video/*'});

    const style: any = useMemo(() => ({

    }), [
        isDragActive,
        isDragReject,
        isDragAccept
    ]);

    const dragOver =(e: any) =>{
        e.preventDefault();
    }
    const dragEnter =(e: any) =>{
        e.preventDefault();
    }
    const dragLeave =(e: any) =>{
        e.preventDefault();
    }
    const fileDrop =(e: any) =>{
        e.preventDefault();
        const files = e.dataTransfer.files;
        console.log(files);
    }

    const validateFile = (file: any) =>{
        const validTypes = ['video/mp4', 'video/mov', 'video/avi', 'video/wmv', 'video/flv'];
        if (validTypes.indexOf(file.type)){
            return false;
        }
        return true;

    }


    return (
        <div>
            <div className="dropbox" {...getRootProps({style})}
                onDragOver={dragOver}
                 onDragEnter={dragEnter}
                 onDragLeave={dragLeave}
                 onDrop={fileDrop}
            >
                <input {...getInputProps()} />
                <p className="Box"> + Ajout Video MP4 </p>
            </div>
        </div>
    );
}

export default Dropzone;