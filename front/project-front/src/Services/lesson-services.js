import http from "../http-common";


const getAll = () => {
    return http.get("/lesson");
};

const get = id => {
    return http.get(`/lesson/${id}`);
};

const create = data => {
    return http.post("/lesson/", data);
};

const update = (id, data) => {
    return http.put(`/lesson/${id}`, data);
};

const remove = id => {
    return http.delete(`/lesson/${id}`);
};

const findByTitle = title => {
    return http.get(`/lesson?title=${title}`);
};

export default {
    getAll,
    create,
    update,
    get,
    remove,
    findByTitle
};
