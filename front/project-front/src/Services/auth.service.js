import http from "../http-common";


const login = async (username, password) => {
    const response = await http
        .post("/signin", {
            username,
            password,
        })
    if (response.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
        localStorage.setItem("access_token", response.data.accessToken);
    }
    return response.data;
};


const logout = () => {
    localStorage.removeItem("user");
    window.location.href = "/";
};

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};

const accueil = () => {
    return http.get("/content-creation");
}

export default {
    login,
    logout,
    getCurrentUser,
    accueil,
};
