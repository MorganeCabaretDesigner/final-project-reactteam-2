import axios from "axios";

class HttpLessonApi {
    http;

    constructor() {
        this.http = axios.create({
            baseURL: 'http://localhost:8080'
        }).interceptors.request.use(function (config) {
            config.headers.Authorization = `Bearer  ${localStorage.getItem("access_token")}`
            return config;
        }, function (error) {
            return Promise.reject(error);
        })
    }

    async save(body) {
        return await this.http.post('/lesson', body);
    }

    getById(body) {
        this.http.get('/lesson/:id', body);
    }

    update(body) {
        this.http.put('/lesson/:id', body);
    }

    delete(body) {
        this.http.delete('/lesson/:id',body);
    }
}

module.exports = new HttpLessonApi();
