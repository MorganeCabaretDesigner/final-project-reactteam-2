import './App.css';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";

import Connexion from "./Components/Log In/connexion";
import CreationPage from "./Components/Lesson Creation/full-page-formation-creation-component";


function App() {

    if (!localStorage.user) {
        return (
            <Connexion/>
        )
    } else {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={Connexion}/>
                    <Route path="/formation-creation" component={CreationPage}/>
                </Switch>
            </Router>
        );
    }
}

export default App;

